﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pokerasa.Models
{
    public class Estado
    {
        public int EstadoID { get; set; }
        public string Descricao { get; set; }
        public string Sigla { get; set; }
        public virtual ICollection<Cidade> Cidades { get; set; }

    }
}
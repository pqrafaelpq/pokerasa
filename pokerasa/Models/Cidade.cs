﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace pokerasa.Models
{
    public class Cidade
    {
        [Key]
        public int CidadeID { get; set; }
        [Required(ErrorMessage = "Preencha o nome da cidade")]
        [DisplayName("Nome da cidade")]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "O nome da cidade deve ter entre 3 e 100 caracteres.")]
        public string Nome { get; set; }
        public int EstadoID { get; set; }
        public virtual Estado Estado { get; set; }
        public virtual ICollection<Casa> Casas { get; set; }
        public virtual ICollection<Jogador> Jogadores { get; set; }
    }

}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace pokerasa.Models
{
    public class Historico
    {

        [Key]
        public int HistoricoID { get; set; }
        public int UsuarioID { get; set; }
        public Usuario Usuario { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DataDoHistorico{ get; set; }
        public int Nota { get; set; }

        public String descricao { get; set; }


    }
}
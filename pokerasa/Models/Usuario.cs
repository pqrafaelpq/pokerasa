﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace pokerasa.Models
{
    public class Usuario
    {
        [Key]
        public int UsuarioID { get; set; }

        [Required(ErrorMessage = "Preencha o nome")]
        [DisplayName("Nome")]
        public String Nome { get; set; }
      

        [Required(ErrorMessage = "Informe um login válido.")]
        [DisplayName("Login")]
        public String Login { get; set; }

        [Required(ErrorMessage = "Digite a senha")]
        [DisplayName("Senha")]
        [DataType(DataType.Password)]
        public String Senha { get; set; }

        public String Tipo { get; set; }

        [Required(ErrorMessage = "Preencha o CPF")]
        [DisplayName("CPF")]
        public String Cpf { get; set; }


        [DataType(DataType.EmailAddress)]
        [Required(ErrorMessage = "Informe um e-mail válido.")]
        [EmailAddress(ErrorMessage = "E-mail inválido")]
        [DisplayName("E-mail")]
        public String Email { get; set; }

        public virtual ICollection<Divida> Dividas { get; set; }
        public virtual ICollection<Historico> Historicos { get; set; }

    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace pokerasa.Models
{
    public class Casa
    {
        [Key]
        public int CasaID { get; set; }
        [Required(ErrorMessage = "Preencha o nome da casa")]
        [DisplayName("Nome da casa")]
        public String NomeDaCasa { get; set; }
        public String Telefone { get; set; }
        [DataType(DataType.EmailAddress)]
        [Required(ErrorMessage = "Informe um e-mail válido.")]
        [EmailAddress(ErrorMessage = "E-mail inválido")]
        [DisplayName("E-mail")]
        public String Email { get; set; }
        [Required(ErrorMessage = "O endereço é obrigatório")]
        [DisplayName("Endereço")]
        public String Endereço { get; set; }
        [Required(ErrorMessage = "Preencha o nome da cidade")]
        [DisplayName("Nome da cidade")]
        public int CidadeID { get; set; }
        public virtual Cidade cidade { get; set; }
        [Required(ErrorMessage = "Preencha o proprietário")]
        [DisplayName("Proprietário")]
        public int UsuarioID { get; set; }
        //fazendo o relacionamento com o Proprietario pra herdar os valores
        public virtual Proprietario proprietario { get; set; }
        //sendo relacionada com o func e divida pra mandar valores
        public virtual ICollection<Funcionario> Funcionarios { get; set; }
        public virtual ICollection<Divida> Dividas { get; set; }

    }
}
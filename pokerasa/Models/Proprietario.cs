﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pokerasa.Models
{
    public class Proprietario : Usuario
    {
        public virtual ICollection<Casa> Casas { get; set; }
    }
}
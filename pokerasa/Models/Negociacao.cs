﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace pokerasa.Models
{
    public class Negociacao
    {

        [Key]

        public int NegociacaoID { get; set; }
        // quando tá sendo feito a negociação
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DataNeg { get; set; }
        //Valor reajustado com aquele jusroszitos
        [DisplayName("Valor")]
        public float ValorAjustado { get; set; }
        // Escolha da Forma de pgt
        [DisplayName("Forma de Pagamento")]
        public string Formapgt { get; set; }

        //fazendo a relação com a dívida pra receber os valores dela.
        public int DividaID { get; set; }
        public virtual Divida Divida { get; set; }
        
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace pokerasa.Models
{
    public class Jogador : Usuario
    {
        [Required(ErrorMessage = "Preencha o documento")]
        [DisplayName("R.G.")]
        public String Rg { get; set; }
    

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DataDeNascimento { get; set; }
      


        public int CidadeID { get; set; }
        public virtual Cidade cidade { get; set; }

        [Required(ErrorMessage = "Preencha o endereço")]
        [DisplayName("Endereço")]
        public String Endereco { get; set; }
   
        [Required(ErrorMessage = "Preencha o telefone")]
        [DisplayName("Telefone")]
        public String Telefone { get; set; }
       

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DataDeCadastro { get; set; }
        



    }
}
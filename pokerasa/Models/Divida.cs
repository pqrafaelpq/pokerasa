﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace pokerasa.Models
{
    public class Divida
    {

        [Key]
        public int DividaID { get; set; }
        public String CpfDoJogador { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DataDeRegistro { get; set; }

        [Required(ErrorMessage = "Preencha o valor")]
        [DisplayName("Valor")]
        public float Valor { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime PrazoNegociacao { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DataFinalizada { get; set; }


        [Required(ErrorMessage = "Informe o motivo da pendência ")]
        [DisplayName("Descrição")]
        public String Descricao { get; set; }

        public string Status { get; set; }

        public int UsuarioID { get; set; }

        public Usuario Usuario { get; set; }

        public int CasaID { get; set; }
        public Casa Casa { get; set; }

        //fazendo a relação com a negociação pra herdar os Valores
        public virtual ICollection<Negociacao> Negociacoes { get; set; }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
namespace pokerasa.Models
{
    public class Context : DbContext
    {
        public Context() : base("pokerasa")
        {
            Configuration.ProxyCreationEnabled = false;
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<Context>());
        }
        public DbSet<Estado> Estados { get; set; }
        public DbSet<Cidade> Cidades { get; set; }        public DbSet<Usuario> Usuarios { get; set; }        public DbSet<Casa> Casas { get; set; }        public DbSet<Divida> Dividas { get; set; }
        public DbSet<Historico> Historicos { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove();
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        public System.Data.Entity.DbSet<pokerasa.Models.Proprietario> Proprietarios { get; set; }

        public System.Data.Entity.DbSet<pokerasa.Models.Administrador> Administradors { get; set; }

        public System.Data.Entity.DbSet<pokerasa.Models.Jogador> Jogadors { get; set; }

        public System.Data.Entity.DbSet<pokerasa.Models.Funcionario> Funcionarios { get; set; }

        public System.Data.Entity.DbSet<pokerasa.Models.Negociacao> Negociacaos { get; set; }
    }
}
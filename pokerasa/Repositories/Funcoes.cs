﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using pokerasa.Models;


using System.Web.Security;
using System.Configuration;

namespace pokerasa.Repositories
{
    public class Funcoes
    {

        public static String EnderecoServidor()
        {
           

            var servidor = ConfigurationManager.AppSettings["servidor"]; ;

            return servidor;
        }

        // função que verifica o usuário logado na Page
        public static bool AutenticarUsuario(string login, string senha)
        {
            Context _db = new Context();
            var query = (from u in _db.Usuarios where u.Login == login && u.Senha == senha select u).SingleOrDefault();
            if (query == null)
            {
                return false;
            }
            FormsAuthentication.SetAuthCookie(query.Login, false);
            //HttpContext.Current.Response.Cookies["Usuario"].Value = query.Email;
            //HttpContext.Current.Response.Cookies["Usuario"].Expires = DateTime.Now.AddDays(10);
            HttpContext.Current.Session["Usuario"] = query.Login;
            return true;
        }
        // Função que identifica o Usuário que está na Page.Se for nulo, retorna valor nulo saindo da func
        public static Usuario GetUsuario()
        {
            string _login = HttpContext.Current.User.Identity.Name;
            //if (HttpContext.Current.Request.Cookies.Count > 0 || HttpContext.Current.Request.Cookies["Usuario"] != null) 
            if (HttpContext.Current.Session.Count > 0 || HttpContext.Current.Session["Usuario"] != null)
            {
                _login = HttpContext.Current.Session["Usuario"].ToString();
                //_login = HttpContext.Current.Request.Cookies["Usuario"].Value.ToString();
                if (_login == "")
                {
                    return null;
                }
                else
                {
                    Context _db = new Context();
                    Usuario usuarios = (from u in _db.Usuarios where u.Login == _login select u).SingleOrDefault();
                    return usuarios;
                }
            }
            else
            {
                return null;
            }
        }
       
            public static Usuario GetUsuario(string _login)
        {
            if (_login == "")
            {
                return null;
            }
            else
            {
                Context _db = new Context();
                Usuario usuarios = (from u in _db.Usuarios where u.Login == _login select u).SingleOrDefault();
                return usuarios;
            }
        }

        //famosa função pra deslogar 
        public static void Deslogar()
        {
            HttpContext.Current.Session["Usuario"] = "";
            //HttpContext.Current.Response.Cookies["Usuario"].Value = "";
            FormsAuthentication.SignOut();
        }
    }
}

namespace pokerasa.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class teste : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Negociacao",
                c => new
                    {
                        NegociacaoID = c.Int(nullable: false, identity: true),
                        DataNeg = c.DateTime(nullable: false, precision: 0),
                        ValorAjustado = c.Single(nullable: false),
                        Formapgt = c.String(unicode: false),
                        DividaID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.NegociacaoID)
                .ForeignKey("dbo.Divida", t => t.DividaID, cascadeDelete: true)
                .Index(t => t.DividaID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Negociacao", "DividaID", "dbo.Divida");
            DropIndex("dbo.Negociacao", new[] { "DividaID" });
            DropTable("dbo.Negociacao");
        }
    }
}

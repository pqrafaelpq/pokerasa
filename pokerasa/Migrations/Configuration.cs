namespace pokerasa.Migrations
{
    using pokerasa.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    //using pokerasa.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<pokerasa.Models.Context>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(pokerasa.Models.Context context)
        {
            IList<Estado> estados = new List<Estado>();
            

            estados.Add(new Estado() { Descricao = "S�o Paulo", Sigla = "SP" });
            estados.Add(new Estado() { Descricao = "Rio de Janeiro", Sigla = "RJ" });
            
         
            foreach (Estado estado in estados)
            {
                context.Estados.AddOrUpdate(x => x.EstadoID, estado);
            }

       
        }

    }
}

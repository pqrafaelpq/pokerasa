﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using pokerasa.Models;
using pokerasa.Repositories;

namespace pokerasa.Controllers
{
    public class NegociacoesController : Controller
    {
        private Context db = new Context();

        // GET: Negociacoes
        public ActionResult Index()
        {
            pokerasa.Models.Usuario userlogado = pokerasa.Repositories.Funcoes.GetUsuario();


            return View(db.Negociacaos.ToList());
        }

        // GET: Negociacoes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Negociacao negociacao = db.Negociacaos.Find(id);
            if (negociacao == null)
            {
                return HttpNotFound();
            }
            return View(negociacao);
        }

        // GET: Negociacoes/Create
        public ActionResult Create(int? id)

        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            pokerasa.Models.Usuario userlogado = pokerasa.Repositories.Funcoes.GetUsuario();

            Divida divida = db.Dividas.Find(id);
            ViewBag.ID = divida.DividaID.ToString();
            ViewBag.Cpf = divida.CpfDoJogador.ToString();
            ViewBag.Valor = divida.Valor.ToString();
            ViewBag.CasaID = divida.CasaID.ToString();
            ViewBag.DataRegistro = divida.DataDeRegistro.ToLongDateString();


            ViewBag.UsuarioID = new SelectList(db.Usuarios.Where(u => u.UsuarioID == userlogado.UsuarioID).ToList(), "UsuarioID", "Nome");
            ViewBag.Servidor = pokerasa.Repositories.Funcoes.EnderecoServidor();
            return View();
        }

        // POST: Negociacoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "NegociacaoID,DataNeg,ValorAjustado,Formapgt")] Negociacao negociacao)
        {
         

            if (ModelState.IsValid)
            {
                db.Negociacaos.Add(negociacao);
                db.SaveChanges();
                


                return RedirectToAction("Index");
            }

            return View(negociacao);
        }
        
    
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Negociacao negociacao = db.Negociacaos.Find(id);
            if (negociacao == null)
            {
                return HttpNotFound();
            }
            return View(negociacao);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "NegociacaoID,DataNeg,ValorAjustado,Formapgt")] Negociacao negociacao)
        {
            if (ModelState.IsValid)
            {
                db.Entry(negociacao).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(negociacao);
        }

        // GET: Negociacoes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Negociacao negociacao = db.Negociacaos.Find(id);
            if (negociacao == null)
            {
                return HttpNotFound();
            }
            return View(negociacao);
        }

        // POST: Negociacoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Negociacao negociacao = db.Negociacaos.Find(id);
            db.Negociacaos.Remove(negociacao);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult SalvarCompra()
        {
            
            int guarino = Convert.ToInt32(Session["NegociacaoID"]);
            pokerasa.Models.Usuario userlogado = pokerasa.Repositories.Funcoes.GetUsuario();
            //  int idconta = Convert.ToInt32(Session["NegociacaoID"]);

           

            Divida divida = db.Dividas.Find(guarino);

            Negociacao negociacao = new Negociacao();
            float valajus = divida.Valor;
            negociacao.DataNeg = DateTime.Now;
            negociacao.ValorAjustado = valajus;
            negociacao.Formapgt = "PayPal";
            negociacao.DividaID = Convert.ToInt32(guarino);

                //int id = Convert.ToInt32(Session["NegociacaoID"]);

            db.Negociacaos.Add(negociacao);
            divida.Status = "Finalizada";
            db.Entry(divida).State = EntityState.Modified;
            db.Entry(divida).Property(s => s.Status).IsModified = true;
            db.SaveChanges();
            
         //   Divida divida2 = db.Dividas.Where(d => d.DividaID == negociacao.DividaID).FirstOrDefault();
            Usuario user = db.Usuarios.Where(u => u.UsuarioID == divida.UsuarioID).FirstOrDefault();
            Jogador jogador = (Jogador)db.Usuarios.Where(u => u.Cpf == divida.CpfDoJogador).FirstOrDefault();

            //envia o email para o proprietário da casa informando o pagamento da dívida
            GmailEmailService gmail = new GmailEmailService();
            EmailMessage msg = new EmailMessage();
            msg.Body = "<img src='http://i64.tinypic.com/6p2aa1.png'> <p>Um jogador acabou de pagar a dívida que você registrou! Em até 48 horas o valor será creditado automaticamente na sua conta!</p>";
            msg.IsHtml = true;
            msg.Subject = "Alguém pagou uma pendência!";
            msg.ToEmail = user.Email.ToString();
            gmail.SendEmailMessage(msg);


            //envia o email para o jogador informando o recebimento da dívida
            GmailEmailService gmailnovo = new GmailEmailService();
            EmailMessage msgnovo = new EmailMessage();
            msgnovo.Body = "<img src='http://i66.tinypic.com/1zxoz8h.png'><p>Estamos confirmando o seu pagamento. Obrigado por negociar conosco! Em breve seu nome não constará mais no cadastro de inadimplentes! </p>";
            msgnovo.IsHtml = true;
            msgnovo.Subject = "Recebemos o seu pagamento!";
            msgnovo.ToEmail = jogador.Email.ToString();
            gmailnovo.SendEmailMessage(msgnovo);




            return RedirectToAction("Home", "Jogadores");
           
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

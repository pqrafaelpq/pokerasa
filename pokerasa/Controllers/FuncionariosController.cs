﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using pokerasa.Models;

namespace pokerasa.Controllers
{
    public class FuncionariosController : Controller
    {
        private Context db = new Context();

        // GET: Funcionarios
        public ActionResult Index()
        {
            var funcionarios = db.Usuarios.OfType<Funcionario>().Include(f => f.casa);
            return View(funcionarios.ToList());
        }

        // GET: Funcionarios/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Funcionario funcionario = (Funcionario)db.Usuarios.Find(id);
            if (funcionario == null)
            {
                return HttpNotFound();
            }
            return View(funcionario);
        }


        public ActionResult Home()
        {

            return View();
        }

        // GET: Funcionarios/Create
        public ActionResult Create()
        {
            ViewBag.CasaID = new SelectList(db.Casas, "CasaID", "NomeDaCasa");
            return View();
        }

        // POST: Funcionarios/Create
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "UsuarioID,CasaID,Nome,Login,Senha,Tipo,Cpf,Email")] Funcionario funcionario)
        {
            if (ModelState.IsValid)
            {
                

                //funcionario.Login = (funcionario.Email).Split('@')[0];
               // string username = (funcionario.Email).Split('@')[0];
               // funcionario.Senha = "xxxxx";
                funcionario.Tipo = "fun";

                if (db.Usuarios.Any(ac => ac.Cpf.Equals(funcionario.Cpf)) || db.Usuarios.Any(ad => ad.Email.Equals(funcionario.Email) || db.Usuarios.Any(ae => ae.Login.Equals(funcionario.Login))))
                {

                   return RedirectToAction("Create");



            //        return Content("<script language='javascript' type='text/javascript'>alert('Usuário já existente!'); window.location.href='@Url.Action('Index','Funcionarios')';</script>");

                }
                else
                {

                    db.Funcionarios.Add(funcionario);
                    db.SaveChanges();
                    //ENVIAR EMAIL AQUI
                    return RedirectToAction("Index");
                }
            }

            ViewBag.CasaID = new SelectList(db.Casas, "CasaID", "NomeDaCasa", funcionario.CasaID);
            return View(funcionario);
        }

        // GET: Funcionarios/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Funcionario funcionario = (Funcionario)db.Usuarios.Find(id);
            if (funcionario == null)
            {
                return HttpNotFound();
            }
            ViewBag.CasaID = new SelectList(db.Casas, "CasaID", "NomeDaCasa", funcionario.CasaID);
            return View(funcionario);
        }

        // POST: Funcionarios/Edit/5
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "UsuarioID,CasaID,Nome,Login,Senha,Tipo,Cpf,Email")] Funcionario funcionario)
        {
            if (ModelState.IsValid)
            {
                db.Entry(funcionario).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CasaID = new SelectList(db.Casas, "CasaID", "NomeDaCasa", funcionario.CasaID);
            return View(funcionario);
        }

        // GET: Funcionarios/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Funcionario funcionario = (Funcionario)db.Usuarios.Find(id);
            if (funcionario == null)
            {
                return HttpNotFound();
            }
            return View(funcionario);
        }

        // POST: Funcionarios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Funcionario funcionario = (Funcionario)db.Usuarios.Find(id);
            db.Funcionarios.Remove(funcionario);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

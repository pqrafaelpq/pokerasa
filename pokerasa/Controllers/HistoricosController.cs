﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using pokerasa.Models;

namespace pokerasa.Controllers
{
    public class HistoricosController : Controller
    {
        private Context db = new Context();

        // GET: Historicos
        public ActionResult Index()
        {
            pokerasa.Models.Usuario userlogado = pokerasa.Repositories.Funcoes.GetUsuario();
            //pega o usuario (jogador) que está logado

            if (userlogado.Tipo == "jog") {
                //le todos os historicos do banco e le todos os jogadores
                var historico = db.Historicos.Include(h => h.Usuario);
                
                //filtra os historicos carregados quando o historico tiver o mesmo CPF do jogador logado
               return View(historico.Where(u => u.Usuario.Cpf == userlogado.Cpf).ToList());
            }

           
            var historicoes = db.Historicos.Include(h => h.Usuario);
            return View(historicoes.ToList());
         

        }

        public ActionResult MeusHistoricos()
        {
            //var historicoes = db.Historicos.Include(h => h.Usuario);
            //return View(historicoes.ToList());

            pokerasa.Models.Usuario userlogado = pokerasa.Repositories.Funcoes.GetUsuario();
            //var dividas = db.Dividas.Include(d => d.Casa).Include(d => d.Usuario);
            
            return View(db.Historicos.Where(u => u.Usuario.Cpf == userlogado.Cpf).ToList());


        }

        // GET: Historicos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Historico historico = db.Historicos.Find(id);
            if (historico == null)
            {
                return HttpNotFound();
            }
            return View(historico);
        }

        // GET: Historicos/Create
        public ActionResult Create(int? id)
        {
            ViewBag.UsuarioID = new SelectList(db.Usuarios.Where(u => u.UsuarioID == id), "UsuarioID", "Nome");
            return View();
        }

        // POST: Historicos/Create
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "HistoricoID,UsuarioID,DataDoHistorico,Nota,descricao")] Historico historico)
        {
            if (ModelState.IsValid)
            {
                db.Historicos.Add(historico);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UsuarioID = new SelectList(db.Usuarios, "UsuarioID", "Nome", historico.UsuarioID);
            return View(historico);
        }

        // GET: Historicos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Historico historico = db.Historicos.Find(id);
            if (historico == null)
            {
                return HttpNotFound();
            }
            ViewBag.UsuarioID = new SelectList(db.Usuarios, "UsuarioID", "Nome", historico.UsuarioID);
            return View(historico);
        }

        // POST: Historicos/Edit/5
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "HistoricoID,UsuarioID,DataDoHistorico,Nota")] Historico historico)
        {
            if (ModelState.IsValid)
            {
                db.Entry(historico).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UsuarioID = new SelectList(db.Usuarios, "UsuarioID", "Nome", historico.UsuarioID);
            return View(historico);
        }

        // GET: Historicos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Historico historico = db.Historicos.Find(id);
            if (historico == null)
            {
                return HttpNotFound();
            }
            return View(historico);
        }

        // POST: Historicos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Historico historico = db.Historicos.Find(id);
            db.Historicos.Remove(historico);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using pokerasa.Models;
using pokerasa.Repositories;

namespace pokerasa.Controllers
{
    public class JogadoresController : Controller
    {
        private Context db = new Context();

        // GET: Jogadores
        public ActionResult Index()
        {
            //var jogadors = db.Usuarios.OfType<Jogador>().Include(j => j.cidade);
            //return View(jogadors.ToList()/*.Where(j => j.Nome=="Macho Virgem")*/   )

            return View();
        }

        [HttpPost]
        public ActionResult SearchNome(FormCollection fc, string searchString)
        {
            ViewBag.Pesquisa = "";
            if (!String.IsNullOrEmpty(searchString))
            {
                ViewBag.Pesquisa = searchString;
                var jogs = db.Usuarios.OfType<Jogador>().Include(j => j.cidade).Where(j => j.Nome.Contains(searchString)).OrderBy(o => o.Nome);

                return View("PesquisarJogadores", jogs.ToList());
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public ActionResult SearchCpf(FormCollection fc, string searchCpf)
        {
            ViewBag.Pesquisa = "";
            if (!String.IsNullOrEmpty(searchCpf))
            {
                ViewBag.Pesquisa = searchCpf;
                var jogs = db.Usuarios.OfType<Jogador>().Include(j => j.cidade).Where(j => j.Cpf.Contains(searchCpf)).OrderBy(o => o.Nome);
                return View("PesquisarJogadores", jogs.ToList());
            }
            else
            {
                return RedirectToAction("Index");
            }
        }


        // GET: Jogadores/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Jogador jogador = (Jogador)db.Usuarios.Find(id);
            if (jogador == null)
            {
                return HttpNotFound();
            }
            return View(jogador);
        }

        // GET: Jogadores/Create
        public ActionResult Create()
        {
            ViewBag.CidadeID = new SelectList(db.Cidades, "CidadeID", "Nome");
            return View();
        }

        public ActionResult Home()
        {

            return View();
        }

        // POST: Jogadores/Create
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "UsuarioID,Rg,DataDeNascimento,CidadeID,Endereco,Telefone,DataDeCadastro,Nome,Login,Senha,Tipo,Cpf,Email")] Jogador jogador)
        {
          //  Jogador jogaCpf = (Jogador)db.Usuarios.Find(jogador.Cpf.ToString());
         //   Jogador jogaMail = (Jogador)db.Usuarios.Find(jogador.Email.ToString());
           
                if (ModelState.IsValid)
                {

                jogador.DataDeCadastro = DateTime.Now;
              
                //jogador.Senha = "xxxxx";
                jogador.Tipo = "jog";
                //jogador.Senha = "12345";

                if (db.Usuarios.Any(ac => ac.Cpf.Equals(jogador.Cpf)) || db.Usuarios.Any(ad => ad.Email.Equals(jogador.Email) || db.Usuarios.Any(ae => ae.Login.Equals(jogador.Login) ) )){

                    return RedirectToAction("Create");

                }
                    else
                {
                   
                    db.Jogadors.Add(jogador);
                    db.SaveChanges();
                    string nomejogador = jogador.Nome;
                    string logindojogador = jogador.Login;
                    
                    string servidor = pokerasa.Repositories.Funcoes.EnderecoServidor();
                    GmailEmailService gmail = new GmailEmailService();
                    EmailMessage msg = new EmailMessage();
                    msg.Body = "<br /><img src='http://i64.tinypic.com/2myq5a1.png'><br /><br /><h1> Bem vindo, "+nomejogador+"!</h1><br /> <b>Você foi cadastrado no Pokerasa! </b> <br />" +
                        "Isso significa que agora você pode construir sua reputação no mundo dos jogos de Poker. Quando você joga, aposta e consome nas <br />" +
                        "casas de poker, é gerado um histórico positivo sobre você. Assim, as casas podem ter a segurança de te oferecer cada vez mais crédito. <br />" +
                        "Mas, se por acaso algum dia você ficar com dívidas, também terá a oportunidade de renegociar suas pendências e ficar em dia para voltar a jogar! <br />" +
                        "Cadastre a sua senha de acesso pelo link abaixo: <br /><b> <a href='"+servidor+"/Jogadores/CriarSenha/"+jogador.UsuarioID+"'>Cadastrar senha de acesso</a> </b> <p>Seu login de acesso é: " + logindojogador+"  </p>";
                    msg.IsHtml = true;
                    msg.Subject = "Bem-Vindo ao Pokerasa";
                    msg.ToEmail = jogador.Email.ToString();
                    gmail.SendEmailMessage(msg);




                    return RedirectToAction("Index");
                }


                   

                }
            

            ViewBag.CidadeID = new SelectList(db.Cidades, "CidadeID", "Nome", jogador.CidadeID);
            return View(jogador);
        }

        // GET: Jogadores/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Jogador jogador = (Jogador)db.Usuarios.Find(id);
            if (jogador == null)
            {
                return HttpNotFound();
            }
            ViewBag.CidadeID = new SelectList(db.Cidades, "CidadeID", "Nome", jogador.CidadeID);
            return View(jogador);
        }

        

        // POST: Jogadores/Edit/5
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "UsuarioID,Rg,DataDeNascimento,CidadeID,Endereco,Telefone,DataDeCadastro,Nome,Login,Senha,Tipo,Cpf,Email")] Jogador jogador)
        {
            if (ModelState.IsValid)
            {
                db.Entry(jogador).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CidadeID = new SelectList(db.Cidades, "CidadeID", "Nome", jogador.CidadeID);
            return View(jogador);
        }


        public ActionResult CriarSenha(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Jogador jogador = (Jogador)db.Usuarios.Find(id);
            if (jogador == null)
            {
                return HttpNotFound();
            }
            
            ViewBag.CidadeID = new SelectList(db.Cidades, "CidadeID", "Nome", jogador.CidadeID);
            return View(jogador);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CriarSenha([Bind(Include = "UsuarioID,Rg,DataDeNascimento,CidadeID,Endereco,Telefone,DataDeCadastro,Nome,Login,Senha,Tipo,Cpf,Email")] Jogador jogador)
        {

            //Jogador jogadoratual = (Jogador)db.Usuarios.Find(jogador.UsuarioID);
            //jogador.CidadeID = jogadoratual.CidadeID;
            //jogador.UsuarioID = jogadoratual.UsuarioID;
            //jogador.Nome = jogadoratual.Nome;
            //jogador.Rg = jogadoratual.Rg;
            //jogador.DataDeCadastro = jogadoratual.DataDeCadastro;
            //jogador.Endereco = jogadoratual.Endereco;
            //jogador.Telefone = jogadoratual.Telefone;
            //jogador.Tipo = jogadoratual.Tipo;
            //jogador.Cpf = jogadoratual.Cpf;
            //jogador.Email = jogadoratual.Email;

            if (ModelState.IsValid)
            {
                db.Entry(jogador).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Logar","Publico");
            }
            ViewBag.CidadeID = new SelectList(db.Cidades, "CidadeID", "Nome", jogador.CidadeID);
            return View(jogador);
        }



        // GET: Jogadores/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Jogador jogador = (Jogador)db.Usuarios.Find(id);
            if (jogador == null)
            {
                return HttpNotFound();
            }
            return View(jogador);
        }

        // POST: Jogadores/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Jogador jogador = (Jogador)db.Usuarios.Find(id);
            db.Jogadors.Remove(jogador);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using pokerasa.Models;

namespace pokerasa.Controllers
{
    public class CasasController : Controller
    {
        private Context db = new Context();

        // GET: Casas
        public ActionResult Index()
        {
            var casas = db.Casas.Include(c => c.cidade).Include(c => c.proprietario);
            return View(casas.ToList());
        }

        // GET: Casas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Casa casa = db.Casas.Find(id);
            if (casa == null)
            {
                return HttpNotFound();
            }
            return View(casa);
        }

        // GET: Casas/Create
        public ActionResult Create()
        {
            ViewBag.CidadeID = new SelectList(db.Cidades, "CidadeID", "Nome");
            ViewBag.UsuarioID = new SelectList(db.Usuarios.OfType<Proprietario>(), "UsuarioID", "Nome");
            return View();
        }

        // POST: Casas/Create
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CasaID,NomeDaCasa,Telefone,Email,Endereço,CidadeID,UsuarioID")] Casa casa)
        {
            if (ModelState.IsValid)
            {
                db.Casas.Add(casa);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CidadeID = new SelectList(db.Cidades, "CidadeID", "Nome", casa.CidadeID);
            ViewBag.UsuarioID = new SelectList(db.Usuarios, "UsuarioID", "Nome", casa.UsuarioID);
            return View(casa);
        }

        // GET: Casas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Casa casa = db.Casas.Find(id);
            if (casa == null)
            {
                return HttpNotFound();
            }
            ViewBag.CidadeID = new SelectList(db.Cidades, "CidadeID", "Nome", casa.CidadeID);
            ViewBag.UsuarioID = new SelectList(db.Usuarios, "UsuarioID", "Nome", casa.UsuarioID);
            return View(casa);
        }

        // POST: Casas/Edit/5
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CasaID,NomeDaCasa,Telefone,Email,Endereço,CidadeID,UsuarioID")] Casa casa)
        {
            if (ModelState.IsValid)
            {
                db.Entry(casa).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CidadeID = new SelectList(db.Cidades, "CidadeID", "Nome", casa.CidadeID);
            ViewBag.UsuarioID = new SelectList(db.Usuarios, "UsuarioID", "Nome", casa.UsuarioID);
            return View(casa);
        }

        // GET: Casas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Casa casa = db.Casas.Find(id);
            if (casa == null)
            {
                return HttpNotFound();
            }
            return View(casa);
        }

        // POST: Casas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Casa casa = db.Casas.Find(id);
            db.Casas.Remove(casa);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using pokerasa.Models;

namespace pokerasa.Controllers
{
    public class AdministradoresController : Controller
    {
        private Context db = new Context();

        // GET: Administradores
        public ActionResult Index()
        {
            return View(db.Usuarios.OfType<Administrador>().ToList());
        }

        public ActionResult Home()
        {

            return View();
        }

        public ActionResult CadastrarNovo()
        {

            return View();
        }


        // GET: Administradores/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Administrador administrador = (Administrador)db.Usuarios.Find(id);
            if (administrador == null)
            {
                return HttpNotFound();
            }
            return View(administrador);
        }

        // GET: Administradores/Create
        public ActionResult Create()
        {
            return View();
        }


        //Feito pra criação de usuários na qual só admin faz

        // POST: Administradores/Create
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "UsuarioID,Nome,Login,Senha,Tipo,Cpf,Email")] Administrador administrador)
        {
            if (ModelState.IsValid)
            {
                administrador.Tipo = "adm";

                if (db.Usuarios.Any(ac => ac.Cpf.Equals(administrador.Cpf)) || db.Usuarios.Any(ad => ad.Email.Equals(administrador.Email) || db.Usuarios.Any(ae => ae.Login.Equals(administrador.Login))))
                {

                    return RedirectToAction("Create");



                  //  return Content("<script language='javascript' type='text/javascript'>alert('Usuário já existente!'); window.location.href='@Url.Action('Index','Administradores')';</script>");

                }
                else
                {
                    db.Usuarios.Add(administrador);
                    db.SaveChanges();
                    return RedirectToAction("Index");

                }

                db.Administradors.Add(administrador);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(administrador);
        }

        //Pesquisa

        // GET: Administradores/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Administrador administrador = (Administrador)db.Usuarios.Find(id);
            if (administrador == null)
            {
                return HttpNotFound();
            }
            return View(administrador);
        }

        //
        // POST: Administradores/Edit/5
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "UsuarioID,Nome,Login,Senha,Tipo,Cpf,Email")] Administrador administrador)
        {
            if (ModelState.IsValid)
            {
                db.Entry(administrador).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(administrador);
        }


        // GET: Administradores/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Administrador administrador = (Administrador)db.Usuarios.Find(id);
            if (administrador == null)
            {
                return HttpNotFound();
            }
            return View(administrador);
        }

        // POST: Administradores/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Administrador administrador = (Administrador)db.Usuarios.Find(id);
            db.Administradors.Remove(administrador);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

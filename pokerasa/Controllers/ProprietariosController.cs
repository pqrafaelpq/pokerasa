﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using pokerasa.Models;

namespace pokerasa.Controllers
{
    public class ProprietariosController : Controller
    {
        private Context db = new Context();

        // GET: Proprietarios
        public ActionResult Index()
        {
            return View(db.Usuarios.OfType<Proprietario>().ToList());
        }

        public ActionResult Home()
        {

            return View();
        }

        // GET: Proprietarios/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Proprietario proprietario = (Proprietario)db.Usuarios.Find(id);
            if (proprietario == null)
            {
                return HttpNotFound();
            }
            return View(proprietario);
        }

        // GET: Proprietarios/Create
        public ActionResult Create()
        {
            
            return View();
        }

        // POST: Proprietarios/Create
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "UsuarioID,Nome,Login,Senha,Tipo,Cpf,Email")] Proprietario proprietario)
        {
            if (ModelState.IsValid)
            {
                proprietario.Tipo = "pro";

                if (db.Usuarios.Any(ac => ac.Cpf.Equals(proprietario.Cpf)) || db.Usuarios.Any(ad => ad.Email.Equals(proprietario.Email) || db.Usuarios.Any(ae => ae.Login.Equals(proprietario.Login))))
                {

                    return RedirectToAction("Create");



                 //   return Content("<script language='javascript' type='text/javascript'>alert('Usuário já existente!'); window.location.href='@Url.Action('Index','Funcionarios')';</script>");

                }
                else
                {
                    db.Usuarios.Add(proprietario);
                    db.SaveChanges();
                    return RedirectToAction("Index");

                }
               
            }
            return View(proprietario);
        }
        // GET: Proprietarios/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Proprietario proprietario = (Proprietario)db.Usuarios.Find(id);
            if (proprietario == null)
            {
                return HttpNotFound();
            }
            return View(proprietario);
        }

        // POST: Proprietarios/Edit/5
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "UsuarioID,Nome,Login,Senha,Tipo,Cpf,Email")] Proprietario proprietario)
        {
            if (ModelState.IsValid)
            {
                db.Entry(proprietario).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(proprietario);
        }

        // GET: Proprietarios/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Proprietario proprietario = (Proprietario)db.Usuarios.Find(id);
            if (proprietario == null)
            {
                return HttpNotFound();
            }
            return View(proprietario);
        }

        // POST: Proprietarios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Proprietario proprietario = (Proprietario)db.Usuarios.Find(id);
            db.Usuarios.Remove(proprietario);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

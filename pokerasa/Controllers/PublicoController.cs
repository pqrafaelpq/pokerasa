﻿using pokerasa.Models;
using pokerasa.Repositories;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

using System.Web;
using System.Web.Mvc;

namespace pokerasa.Controllers
{
    public class PublicoController : Controller
    {
        private Context db = new Context();
        // GET: Publico
        public ActionResult Logar()
        {
          
            ViewBag.Servidor = pokerasa.Repositories.Funcoes.EnderecoServidor();
            return View();
        }

        public ActionResult Redefinir()
        {

            return View();
        }

        //Action para redefinir a senha com gmail service parecido com o SearchCpf do Dividascontroller
        [HttpPost]
        public ActionResult Redefinir(FormCollection fc, string Redefinir)
        {
            ViewBag.Pesquisa = "";
            if (!String.IsNullOrEmpty(Redefinir))
            {
                ViewBag.Pesquisa = Redefinir;
                ViewBag.Mensagem = "";


                Usuario jogador = db.Usuarios.Where(u => u.Cpf == Redefinir).FirstOrDefault();

                if (jogador == null)
                {

                    return RedirectToAction("NadaEncontrado");

                }
                string servidor = pokerasa.Repositories.Funcoes.EnderecoServidor();
                GmailEmailService gmail = new GmailEmailService();
                EmailMessage msg = new EmailMessage();
                msg.Body = "<p>Uma solicitação de redefinição de senha foi feita. Clique no link: " + " <a href='"+servidor+"/Jogadores/CriarSenha/" + jogador.UsuarioID + "'>Cadastrar senha de acesso</a> </b>  </p>";
                msg.IsHtml = true;
                msg.Subject = "Redefina sua senha"; //Assunto do Email
                msg.ToEmail = jogador.Email.ToString(); //Email do jogador
                gmail.SendEmailMessage(msg);
                return RedirectToAction("Logar");
            }
            return RedirectToAction("Logar");

        }



        public ActionResult LogarAjax(string email, string senha)
        {

       //     String resultado = "";

            if (Funcoes.AutenticarUsuario(email, senha) == false)
            {
                ViewBag.Error = "Dados de acesso inválidos.";
                return Json("err", JsonRequestBehavior.AllowGet);
            }
            else
            {

                Usuario usuario = pokerasa.Repositories.Funcoes.GetUsuario();

                if (usuario.Tipo == "adm")
                {
                    return Json("adm", JsonRequestBehavior.AllowGet);

                }

                if (usuario.Tipo == "fun")
                {
                    return Json("fun", JsonRequestBehavior.AllowGet);
                }

                if (usuario.Tipo == "jog")
                {
                    return Json("jog", JsonRequestBehavior.AllowGet);
                }

                if (usuario.Tipo == "pro")
                {
                    return Json("pro", JsonRequestBehavior.AllowGet);
                }


            }

            return Json("err", JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult Logar(string email, string senha)
        {

            if (email==null || senha == null)
            {

                ViewBag.Error = "O preenchimento dos dados é obrigatório!";
                return View();
            } else { 


            if (Funcoes.AutenticarUsuario(email, senha) == false)
            {
                ViewBag.Error = "Dados de acesso inválidos.";
                return View();
            }
            else
            {

                Usuario usuario = pokerasa.Repositories.Funcoes.GetUsuario();

                if (usuario.Tipo == "adm")
                {
                    return RedirectToAction("Home", "Administradores");

                }

                if (usuario.Tipo == "fun")
                {
                    return RedirectToAction("Home", "Funcionarios");
                }

                if (usuario.Tipo == "jog")
                {
                    return RedirectToAction("Home", "Jogadores");
                }

                if (usuario.Tipo == "pro")
                {
                    return RedirectToAction("Home", "Proprietarios");
                }


            }
            }
            return RedirectToAction("Index", "Home");




        }

        public ActionResult AcessoNegado()
        {
            using (Context c = new Context())
            {
                return View();
            }
        }

        public ActionResult Logoff()
        {
            pokerasa.Repositories.Funcoes.Deslogar();
            return RedirectToAction("Logar", "Publico");
        }

    }
}
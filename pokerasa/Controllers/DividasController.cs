﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using pokerasa.Models;
using pokerasa.Repositories;

namespace pokerasa.Controllers
{
    public class DividasController : Controller
    {
        private Context db = new Context();

        // GET: Dividas
        public ActionResult Index()
        {

            pokerasa.Models.Usuario userlogado = pokerasa.Repositories.Funcoes.GetUsuario();



            var dividas = db.Dividas.Include(d => d.Casa).Include(d => d.Usuario);


            if (userlogado.Tipo == "pro")
            {
                return View(db.Dividas.Where(u => u.UsuarioID == userlogado.UsuarioID).ToList());
            }
            else
            {
                return View(db.Dividas.ToList());


            }

        }

        public ActionResult VerDividas()
        {

            pokerasa.Models.Usuario userlogado = pokerasa.Repositories.Funcoes.GetUsuario();
            
            var dividas = db.Dividas.Include(d => d.Casa).Include(d => d.Usuario);
           
            return View(db.Dividas.Where(u => u.CpfDoJogador == userlogado.Cpf).ToList());
           
            //.Where(u => u.UsuarioID == userlogado.UsuarioID).
        }

        // GET: Dividas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Divida divida = db.Dividas.Find(id);
            if (divida == null)
            {
                return HttpNotFound();
            }


            Usuario user = db.Usuarios.Find(divida.UsuarioID);
            Casa casa = db.Casas.Find(divida.CasaID);
            Jogador jogador = (Jogador)db.Usuarios.Where(u => u.Cpf == divida.CpfDoJogador).FirstOrDefault();

            ViewBag.NomeDoJogador = jogador.Nome;


            return View(divida);
        }

        // GET: Dividas/Create
        public ActionResult Create(int? id)

        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Usuario user = db.Usuarios.Find(id);

            ViewBag.Cpf = user.Cpf.ToString();
            ViewBag.NomeJogador = user.Nome.ToString();
            //mandando o ID da casa e o Nome.
            pokerasa.Models.Usuario userlogado = pokerasa.Repositories.Funcoes.GetUsuario();
            ViewBag.CasaID = new SelectList(db.Casas, "CasaID", "NomeDaCasa");
            //retorna o usuário logado que cadastrou a divida
            ViewBag.UsuarioID = new SelectList(db.Usuarios.Where(u => u.UsuarioID == userlogado.UsuarioID).ToList(), "UsuarioID", "Nome");
            return View();
        }
        // POST: Dividas/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DividaID,CpfDoJogador,DataDeRegistro,Valor,PrazoNegociacao,DataFinalizada,Descricao,Status,UsuarioID,CasaID")] Divida divida)
        {

            pokerasa.Models.Usuario userlogado = pokerasa.Repositories.Funcoes.GetUsuario();

            //divida.Usuario = userlogado;
            //divida.UsuarioID = userlogado.UsuarioID;
            string prazo = Request.Form["YourRadioButton"];
            string Cpf = Request.Form["CpfJogador"];
            DateTime today = DateTime.Now;
            if (prazo == "1")
            {

                divida.PrazoNegociacao = today.AddDays(5);

            }

            if (prazo == "2")
            {
                divida.PrazoNegociacao = today.AddDays(10);

            }

            if (prazo == "3")
            {
                divida.PrazoNegociacao = today.AddDays(15);

            }


            divida.Status = "Prazo inicial";
            divida.CpfDoJogador = Cpf;
            divida.DataFinalizada = DateTime.Now;
            divida.DataDeRegistro = DateTime.Now;


            if (ModelState.IsValid)
            {
                db.Dividas.Add(divida);
                db.SaveChanges();

                Jogador jogador = (Jogador)db.Usuarios.Where(u => u.Cpf == divida.CpfDoJogador).FirstOrDefault();

                GmailEmailService gmail = new GmailEmailService();
                EmailMessage msg = new EmailMessage();
                msg.Body = "<img src='http://i65.tinypic.com/a4murd.png'><p>Uma nova pendência foi registrada em seu nome no valor de: R$ " + divida.Valor+". Acesse agora a plataforma Pokerasa para sanar sua pendência e manter uma boa reputação entre os jogos de Poker! </p>";
                msg.IsHtml = true;
                msg.Subject = "Uma nova pendência foi registrada!";
                msg.ToEmail = jogador.Email.ToString();
                gmail.SendEmailMessage(msg);

                

                return RedirectToAction("Index");
            }

            ViewBag.CasaID = new SelectList(db.Casas, "CasaID", "NomeDaCasa", divida.CasaID);
            ViewBag.UsuarioID = new SelectList(db.Usuarios, "UsuarioID", "Nome", divida.UsuarioID);
            return View(divida);
        }

        // GET: Dividas/Edit/5

        public ActionResult NadaEncontrado()
        {

            return View();
        }

        public ActionResult ConsultarCadastro()
        {

            return View();
        }


     //   [HttpPost]
        //public ActionResult SearchCpf(FormCollection fc, string searchCpf)
        public ActionResult SearchCpf(string searchCpf)
        {
            ViewBag.Pesquisa = "";
            if (!String.IsNullOrEmpty(searchCpf))
            {
                ViewBag.Pesquisa = searchCpf;
                ViewBag.Mensagem = "";


                Jogador jogador = (Jogador)db.Usuarios.Where(u => u.Cpf == searchCpf).FirstOrDefault();

                if (jogador == null) //SE NENHUM JOGADOR FOR ENCONTRADO
                {

                    //  return RedirectToAction("NadaEncontrado");
                    //AO INVÉS DE CARREGAR UMA P´´AGINA INTEIRA....

                    var buscane = new {
                        status = "ne",
                        nota = "XX"
                    };
                    return Json(buscane, JsonRequestBehavior.AllowGet);
                    //ELE RETORNA APENAS ALGUNS BYTES PRO JAVASCRIPT PROCESSAR

                }
                
                var historicos = db.Historicos.Include(h => h.Usuario).Where(d => d.UsuarioID == jogador.UsuarioID);
             
                if (historicos.Count() > 0)
                {
                    double media = db.Historicos.Include(h => h.Usuario).Where(d => d.UsuarioID == jogador.UsuarioID).Select(h => h.Nota).Average();
                    int contagem = db.Historicos.Include(h => h.Usuario).Where(d => d.UsuarioID == jogador.UsuarioID).Select(h => h.Nota).Count();
                    ViewBag.Media = Convert.ToString(media);
                    ViewBag.Quantos = Convert.ToString(contagem);

                }else
                {
                    ViewBag.Media = "0";
                    ViewBag.Quantos = "nenhuma";

                }

              
                if (db.Dividas.Any(d => d.CpfDoJogador == jogador.Cpf))
                {
                    var dividas = db.Dividas.Where(d => d.CpfDoJogador == jogador.Cpf);

                    if (dividas.Any(d => d.Status != "Finalizada"))
                    {

                        // ViewBag.Mensagem = "Pendências foram detectadas no sistema. ";
                      ViewBag.pen = "sim";

                    }
                    else
                    {
                        //   ViewBag.Mensagem = "Nenhuma dívida encontrada!";
                        //321.144.548-95
                        ViewBag.Pen = "limpo";
                    }

                } else
                {

                    ViewBag.Pen = "limpo";
                }


                ViewBag.Nome = jogador.Nome;

                //     return View("SituacaoJogador", historicos.ToList());
                var busca = new
                {
                    status = ViewBag.Pen,
                    nota = ViewBag.Media,
                    nome = jogador.Nome,
                    cadastrado = jogador.DataDeCadastro.ToString()
                };
                return Json(busca, JsonRequestBehavior.AllowGet);


            }

            var busca2 = new
            {
                status = "ne",
                nota = "xx"
            };
            return Json(busca2, JsonRequestBehavior.AllowGet);

        }




        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Divida divida = db.Dividas.Find(id);
            if (divida == null)
            {
                return HttpNotFound();
            }
            pokerasa.Models.Usuario userlogado = pokerasa.Repositories.Funcoes.GetUsuario();
            ViewBag.CasaID = new SelectList(db.Casas, "CasaID", "NomeDaCasa");
            ViewBag.UsuarioID = new SelectList(db.Usuarios.Where(u => u.UsuarioID == userlogado.UsuarioID).ToList(), "UsuarioID", "Nome");
            return View(divida);
        }

        // POST: Dividas/Edit/5
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "DividaID,CpfDoJogador,DataDeRegistro,Valor,PrazoNegociacao,DataFinalizada,Descricao,Status,UsuarioID,CasaID")] Divida divida)
        {
            if (ModelState.IsValid)
            {
                db.Entry(divida).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CasaID = new SelectList(db.Casas, "CasaID", "NomeDaCasa", divida.CasaID);
            ViewBag.UsuarioID = new SelectList(db.Usuarios, "UsuarioID", "Nome", divida.UsuarioID);
            return View(divida);
        }

        // GET: Dividas/Delete/5


     


        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Divida divida = db.Dividas.Find(id);
            if (divida == null)
            {
                return HttpNotFound();
            }
            return View(divida);
        }

        public ActionResult FinalizarDivida(int id)
        {
            int dividaID = id;
         
                Divida divida = db.Dividas.Find(dividaID);
                divida.Status = "Finalizada";
                db.Entry(divida).State = EntityState.Modified;
                db.Entry(divida).Property(s => s.Status).IsModified = true;
                db.SaveChanges();

            //EMAIL SALVO

               // return Json(true);
                return RedirectToAction("Index");
          
        }


        // POST: Dividas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Divida divida = db.Dividas.Find(id);
            db.Dividas.Remove(divida);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
